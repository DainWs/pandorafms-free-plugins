' df_Smart.vbs
' Returns status of all disk
' For PandoraFMS Plugin, (c) 2019 Jose Antonio Duarte
' ----------------------------------------------------

Option Explicit
On Error Resume Next

' Variables
Dim searchWMIObject, objectList, objectData

' Get disk information
Set searchWMIObject = GetObject ("winmgmts:\\.\root\cimv2")
Set objectList = searchWMIObject.ExecQuery ("Select * from Win32_DiskDrive")

For Each objectData in objectList
	Wscript.StdOut.WriteLine "<module>"
	Wscript.StdOut.WriteLine "    <name><![CDATA[DiskFree%_" & objectData.Index & "]]></name>"
	Wscript.StdOut.WriteLine "    <description><![CDATA[The status of the disk with serial number : " & objectData.Model & "]]></description>"
	Wscript.StdOut.WriteLine "    <type>generic_data_string</type>"
	Wscript.StdOut.WriteLine "    <data><![CDATA[" & objectData.status & "]]></data>"
	Wscript.StdOut.WriteLine "    <str_critical>FAIL</str_critical>"
	Wscript.StdOut.WriteLine "</module>"
	Wscript.StdOut.flush
Next
